import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link
  } from "react-router-dom";

import About from './About'
import Home from './Home'
import Users from './Users'
  import React from 'react'
import Login from "./Login";

  export default function Routers() {
	  return (
		<Router >
		<div>
			<div style={{height:40,fontSize:24,backgroundColor:"#318682"}}>Sample Pro</div>
		 
			<div style={{display:"flex",flexDirection:"row",background:"#318666",margin:0}}>
			  <div style={{marginLeft:10,backgroundColor:"#318682"}}>
				<Link to="/home">Home</Link>
			  </div>
			  <div style={{marginLeft:10,backgroundColor:"#318682"}}>

				<Link to="/about">About</Link>
			  </div>
			  <div style={{marginLeft:10,backgroundColor:"#318682"}}>

			  <Link to="/users">Users</Link>
			  </div>
			</div>
		 
  
		  {/* A <Switch> looks through its children <Route>s and
			  renders the first one that matches the current URL. */}
		  <Switch>
			<Route exact={true} path="/about">
			  <About />
			</Route>
			<Route  exact={true}  path="/users">
			  <Users />
			</Route>
			<Route  exact={true}  path="/">
			  <Home />
			</Route>
			<Route  exact={true}  path="/login">
				<Login ></Login>
			</Route>
		  </Switch>
		</div>
	  </Router>
	  )
  }
  