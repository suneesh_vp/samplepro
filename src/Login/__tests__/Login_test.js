import React from "react"
import { shallow,mount}from "enzyme"

import Login from "../index"
import renderer from "react-test-renderer"

import {BrowserRouter} from "react-router-dom"


it("renders without crashing", () => {
    shallow(<BrowserRouter><Login /></BrowserRouter>);
});

it("renders fullDOM without crashing", () => {
    mount(<BrowserRouter><Login /></BrowserRouter>);
});

it("renders correctly Dashboard component", () => {
    const Component = renderer.create(<BrowserRouter><Login /></BrowserRouter>).toJSON();
    expect(Component).toMatchSnapshot();
});