import React from 'react'
import { Form, Input, Button, Checkbox } from 'antd';
import { useHistory } from "react-router-dom";

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

const Login = () => {
	const history=useHistory()
	
  const onFinish = values => {
	console.log('Success:', values);
	history.push("home")
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  return (<div style={{flex:1,display:"flex",backgroundColor:"#FFFFFD"}}>
    <Form
      {...layout}
     
      initialValues={{ remember: true }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <Form.Item
        label="Username"
       name="email"
        rules={[{ required: true, message: 'Please input your username!' }]}
      >
        <Input id="email" />
      </Form.Item>

      <Form.Item
        label="Password"
       
        rules={[{ required: true, message: 'Please input your password!' }]}
      >
        <Input.Password   id="password"/>
      </Form.Item>

      <Form.Item {...tailLayout} name="remember" valuePropName="checked">
        <Checkbox>Remember me</Checkbox>
      </Form.Item>

      <Form.Item {...tailLayout} id="login">
        <Button  type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
	</div>
  );
};
export default Login